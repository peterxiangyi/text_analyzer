import os
import pandas as pd
import sys
from tqdm.auto import tqdm
import json


def getfiles(input_path, accepted_formats = (".json")):
    json_file_paths = []
    if os.path.isdir(input_path):
        for filename in os.listdir(input_path):
            if filename.lower().endswith(accepted_formats): 
                json_file_paths.append(os.path.join(input_path, filename))
    else:
        if input_path.lower().endswith(accepted_formats): 
            json_file_paths.append(input_path)
    return json_file_paths


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)
        
        
def is_big_text(text, threshold=1000):
    """
    this will filter out text object with too small size 

    Args:
        text (json): json object with extracted texts
        threshold (int, optional): text object is small if area less than it. Defaults to 1000.

    Returns:
        bollean: is true if area is larger than threshold.
    """
    if text['w'] != None and text['h'] != None:
        return text['w'] * text['h'] > threshold
    else:
        return None
    
    
def get_training_data(training_data_file):
    """
    reading training data

    Args:
        training_data_file (csv): labelled training data

    Returns:
        DataFrame: labelled training data, with two columns (Text, Cat)
    """
    close = False
    try:
        return pd.read_csv(training_data_file)
    except (FileNotFoundError, IsADirectoryError) as e:
        print("input should be a path to csv data file:", e)
        close = True
    if close:
        sys.exit(1)
        
        
def make_docs_multiclass(data, nlp, unique_labels):
    """
    this will take a list of texts and labels and transform them in spacy documents
    
    data: List(str)
    nlp: spacy lang object
    labels: List(labels)
    
    returns: List(spacy.Doc.doc)
    """
    
    docs = []

    # nlp.pipe([texts]) is way faster than running nlp(text) for each text
    # as_tuples allows us to pass in a tuple, the first one is treated as text
    # the second one will get returned as it is.
    
    for doc, label in tqdm(nlp.pipe(data, as_tuples=True), total = len(data)):
        label_dict = {label: False for label in unique_labels}
        # we need to set the (text)cat(egory) for each document
        doc.cats = label_dict
        doc.cats[label] = True

        # put them into a nice list
        docs.append(doc)
    
    return docs


def show_results(model_path):
    """
    to show the  model performance

    Args:
        model_path (path): the directory name of trained best-model
    """
    meta_path = os.path.join(model_path, "model-best/meta.json")
                             
    with open(meta_path) as json_file:
        metrics = json.load(json_file)
        
    performance = metrics['performance']

    score = performance['cats_score']
    auc = performance['cats_macro_auc']
    f1 = performance['cats_macro_f']
    precision = performance['cats_macro_p']
    recall = performance['cats_macro_r']
    overall_dict = {'score': score, 'precision': precision, 'recall': recall, 'F1': f1, 'AUC': auc}
    overall_df = pd.DataFrame(overall_dict, index=[0])
    
    print('\nThe overall performance:')
    print(overall_df)
    
    # break down the metrics into specific categories, which are saved as the values of the cats_f_per_type key of performance
    print('\nThe performance across different types:')
    per_cat_dict = performance['cats_f_per_type']
    per_cat_df = pd.DataFrame(per_cat_dict)
    print(per_cat_df)
    
    
def text_extraction(input_path, folder_name):
    folder_path = os.path.join(input_path, folder_name)
    text_file_names = getfiles(folder_path)

    text_list = []
    for file_name in text_file_names:
        try:
            # Opening JSON file and return JSON object as a dict
            f = open(file_name)
            data = json.load(f)
            
            text_list_temple = []

            for text in data['texts']:
                text_list_temple.append(text['text'])
                
            text_list.extend(text_list_temple)
        except IOError as e:
            print("Unable to open file. %s" % e)

    df = pd.DataFrame(text_list)
    print(df.head())
    df.to_csv(os.path.join(input_path,'text_' + folder_name + '.csv'), encoding='utf-8-sig')