import os
import argparse
from utils import get_training_data, make_docs_multiclass, show_results, mkdir

import spacy
from sklearn.model_selection import train_test_split
from spacy.tokens import DocBin


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='a path to labelled training csv data file', type=str)
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    data_file = args.input
    text_banner = get_training_data(args.input)
    
    nlp=spacy.load("zh_core_web_md")
    
    text_banner.columns = ["Text", "Cat"]
    # spaCy require the category be str instead of int
    text_banner['Cat'] = text_banner['Cat'].apply(str)  
    
    # 将dataframe转换成一个tuples列表
    text_banner['tuples'] = text_banner.apply(lambda row: (row['Text'], row['Cat']), axis=1)
    dataset =text_banner['tuples'].tolist()
    
    train_data, valid_data = train_test_split(dataset, test_size=0.2, random_state=1)
    
    unique_labels = text_banner['Cat'].unique().tolist()
    
    spacy_path = "./output-multiclass"
    if not os.path.isdir(spacy_path):
        mkdir(spacy_path)
    
    valid_docs = make_docs_multiclass(valid_data, nlp, unique_labels)
    doc_bin = DocBin(docs=valid_docs)
    doc_bin.to_disk(os.path.join(spacy_path, 'valid.spacy'))
    train_docs = make_docs_multiclass(train_data, nlp, unique_labels)
    doc_bin = DocBin(docs=train_docs)
    doc_bin.to_disk(os.path.join(spacy_path, 'train.spacy'))
    
    # run training progress
    command_path = os.path.join("python -m spacy train ./config.cfg --output" + " " + spacy_path )
    # os.system("python -m spacy train ./config.cfg --output ./output-multiclass")
    os.system(command_path)
    
    # show the result
    print(spacy_path)
    show_results(spacy_path)