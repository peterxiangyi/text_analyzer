from utils import text_extraction


if __name__ == "__main__":
    
    input_path = './data'
    folder_name_list = ['bed', 'cosmetics', 'furniture']
    
    for folder_name in folder_name_list:
        text_extraction(input_path, folder_name)
    