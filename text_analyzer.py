import os
import argparse
from tqdm import tqdm
from utils import getfiles, mkdir, is_big_text

import spacy
import json


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='a json file path, or a directory name', type=str)
    parser.add_argument("-o", "--output", help='path to output directory', type=str, default='')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    json_file_paths = getfiles(args.input)
    assert len(json_file_paths), "input should be a path to json file or directory."
    
    result_dir = os.path.join(args.output, 'results')
    mkdir(result_dir)
    output_file = os.path.join(result_dir, 'text_cat_results' + '.json')
    
    model_path = "./output-multiclass"
    
    # set progress bar
    pbar = tqdm(total=len(json_file_paths))
    
    text_cat_files = []
    for json_file_path in json_file_paths:
        file_name = os.path.splitext(os.path.basename(json_file_path))[0]
        
        meta_path = os.path.join(model_path, "model-best")
        nlp = spacy.load(meta_path)
        
        with open(json_file_path) as f:
            json_data = json.load(f)
            
            text_cat = []
            text_cont = []
            for text in json_data['texts']:
                if (is_big_text(text)):
                    text_cont.append(text['text'])
                    doc = nlp(text['text'])
                    # get the first element of sorted classification data
                    result = next(iter(dict(sorted(doc.cats.items(), key=lambda item: item[1], reverse=True))))
                    text_cat.append(result)
                
        final_result = {}
        final_result['file_name'] = file_name
        text_cat_result = dict(zip(text_cont, text_cat))
        final_result['cat'] = text_cat_result
        
        text_cat_files.append(final_result)
        
        pbar.update(1)
       
       
    # save to JSON file
    with open(output_file, 'w', encoding='utf8') as f:
        f.write(json.dumps(text_cat_files, ensure_ascii=False, indent=1))
