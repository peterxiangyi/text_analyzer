# Text Classification Analysis
##################################

## Pre-Requirements
* OCR (Robin's work)
    * https://pytorch.org/get-started/locally/
    * for example, CUDA 11.1 & Torch 1.8
        ```python
        pip install torch==1.8.2+cu111 torchvision==0.9.2+cu111 torchaudio===0.8.2 -f https://download.pytorch.org/whl/lts/1.8/torch_lts.html


## Requirements
# create vatural environment
    * for example
    """
        conda create -n AdBanner python=3.9
    """

# install packages
* pandas
    * https://pypi.org/project/pandas/
    * for example
    """
        pip install pandas
    """

* sklearn
    * https://scikit-learn.org/stable/install.html
    * for example, 
    """
        pip install -U scikit-learn
    """

* tqdm
    * https://pypi.org/project/tqdm/
    * for example
    """
        pip install tqdm
    """

* spaCy
    * https://spacy.io/usage
    * for example, macOS & CPU
    """
        pip install -U pip setuptools wheel
        pip install -U spacy
        python -m spacy download zh_core_web_md
    """

* utils.py (under same folder)


## Text Classification:  
    * Before using text classification function below, please make sure all images have been analyzied by OCR function and output .json files are saved in target folder.
    * The text object with area less than threshold (default 1000) are omitted.
    * input: a json file path, or a directory name
    * **-o, --output**: path to output directory
```python
python text_analyzer.py ./data/bed -o ./results/bed
```

## Model training and re-training
    * read training file do transformation, and training.
    * input: training data file in csv format.
```python
python text_classification_training.py ./data/labelled_texts_all.csv
```


